php-stomp-5 (1:1.0.9-1) unstable; urgency=medium

  * New upstream version 1.0.9
  * Split the source package for PHP 5.x

 -- Ondřej Surý <ondrej@debian.org>  Wed, 06 Jul 2022 15:42:14 +0200

php-stomp (2.0.2+1.0.9-15) unstable; urgency=medium

  * Fix the d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Mon, 03 Jan 2022 15:01:35 +0100

php-stomp (2.0.2+1.0.9-14) unstable; urgency=medium

  * Regenerate d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Sat, 01 Jan 2022 13:09:59 +0100

php-stomp (2.0.2+1.0.9-12) unstable; urgency=medium

  * Recompile for PHP 7.4 until the transition is complete

 -- Ondřej Surý <ondrej@debian.org>  Fri, 26 Nov 2021 11:26:06 +0100

php-stomp (2.0.2+1.0.9-11) unstable; urgency=medium

  * Update the packaging to dh-php >= 4~

 -- Ondřej Surý <ondrej@debian.org>  Wed, 10 Nov 2021 12:01:07 +0100

php-stomp (2.0.2+1.0.9-10) unstable; urgency=medium

  * Bump B-D to dh-php >= 3.1~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 18:08:09 +0100

php-stomp (2.0.2+1.0.9-9) unstable; urgency=medium

  * Override the PHP_DEFAULT_VERSION

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 17:08:12 +0100

php-stomp (2.0.2+1.0.9-8) unstable; urgency=medium

  * Revert arch:all change, as it breaks shlibs:Depends

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 13:21:50 +0100

php-stomp (2.0.2+1.0.9-7) unstable; urgency=medium

  * Use standard d/gbp.conf
  * The main dummy package is arch:all
  * Bump dh-php Build-Depends to >= 3.0~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 07:51:28 +0100

php-stomp (2.0.2+1.0.9-6) unstable; urgency=medium

  * Sync the changelog with Debian bullseye
  * Update d/gbp.conf for debian/main branch
  * Update standards version to 4.5.1 (no change)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Feb 2021 12:11:08 +0100

php-stomp (2.0.2+1.0.9-5) unstable; urgency=medium

  * Update d/watch to use https://pecl.php.net
  * Lower the dh-php dependency to Debian sid version
  * Update d/gbp.conf for Debian bullseye

 -- Ondřej Surý <ondrej@debian.org>  Sun, 14 Feb 2021 16:41:23 +0100

php-stomp (2.0.2+1.0.9-4) unstable; urgency=medium

  * Finish conversion to debhelper compat level 10
  * Update for dh-php >= 2.0 support
  * Build the extension only for PHP 5 and PHP 7

 -- Ondřej Surý <ondrej@debian.org>  Sat, 17 Oct 2020 07:06:36 +0200

php-stomp (2.0.2+1.0.9-3) unstable; urgency=medium

  * No change rebuild for Debian buster

 -- Ondřej Surý <ondrej@debian.org>  Wed, 28 Aug 2019 09:22:45 +0200

php-stomp (2.0.2+1.0.9-2) unstable; urgency=medium

  * Bump the required dh-php version to >= 0.33~

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Oct 2018 12:10:27 +0000

php-stomp (2.0.2+1.0.9-1) unstable; urgency=medium

  * New upstream version 2.0.2+1.0.9
  * Update maintainer address to team address

 -- Ondřej Surý <ondrej@debian.org>  Wed, 03 Oct 2018 07:32:27 +0000

php-stomp (2.0.1+1.0.9-4) unstable; urgency=medium

  * Debian Maintainer field MUST have a name

 -- Ondřej Surý <ondrej@debian.org>  Tue, 06 Feb 2018 15:35:37 +0000

php-stomp (2.0.1+1.0.9-3) unstable; urgency=medium

  * Update the maintainer address to the package address and add myself to
    Uploaders.

 -- Ondřej Surý <ondrej@debian.org>  Tue, 06 Feb 2018 08:45:53 +0000

php-stomp (2.0.1+1.0.9-2) unstable; urgency=medium

  * Update the Vcs-* links to salsa.d.o

 -- Ondřej Surý <ondrej@debian.org>  Tue, 06 Feb 2018 08:10:09 +0000

php-stomp (2.0.1+1.0.9-1) unstable; urgency=medium

  * New upstream version 2.0.1+1.0.9
  * Remove patches for PHP 7.1 support, they have been included upstream

 -- Ondřej Surý <ondrej@debian.org>  Tue, 30 May 2017 10:17:34 +0200

php-stomp (2.0.0+1.0.9-3) unstable; urgency=medium

  * Use patches from Remi's repo to add PHP 7.1 support

 -- Ondřej Surý <ondrej@debian.org>  Wed, 15 Mar 2017 23:06:09 +0100

php-stomp (2.0.0+1.0.9-2) unstable; urgency=medium

  * Disable PHP 7.1.x builds

 -- Ondřej Surý <ondrej@debian.org>  Mon, 07 Nov 2016 10:24:35 +0100

php-stomp (2.0.0+1.0.9-1) unstable; urgency=medium

  * Imported Upstream version 2.0.0+1.0.9
  * Remove PHP 7 compatibility patch

 -- Ondřej Surý <ondrej@debian.org>  Mon, 07 Nov 2016 10:01:01 +0100

php-stomp (1.0.9-3) unstable; urgency=medium

  * Move the default makefile snippet to dh-php and use a simple d/rules
    with dh-php >= 0.12~

 -- Ondřej Surý <ondrej@debian.org>  Fri, 29 Apr 2016 11:19:48 +0200

php-stomp (1.0.9-2) unstable; urgency=medium

  * Change gbp branches to master/upstream/pristine-tar
  * Improve d/rules so it can be used with both single or multiple PECL
    upstream versions, controlled by looking for package-MAJOR.MINOR.xml,
    package-MAJOR.xml and package.xml in this order

 -- Ondřej Surý <ondrej@debian.org>  Thu, 28 Apr 2016 16:30:24 +0200

php-stomp (1.0.9-1) unstable; urgency=medium

  [ David Prévot ]
  * Fix team PEAR -> PECL

  [ Ondřej Surý ]
  * Team upload
  * Imported Upstream version 1.0.9
  * Convert packaging to PHP 7
  * Add compatibility patch for PHP 7
  * Override license-problem-php-license lintian error

 -- Ondřej Surý <ondrej@debian.org>  Mon, 04 Apr 2016 00:21:39 +0200

php-stomp (1.0.6-1) unstable; urgency=low

  * Team upload.
  * Imported Upstream version 1.0.6
  * Switched package helper to dh-php5
  * d/control:
   - bumped standards version to 3.9.6 (no changes needed)
   - changed Homepage to https
   - switched my mail address to @debian.org

 -- Jonas Genannt <genannt@debian.org>  Sat, 02 May 2015 18:58:43 +0200

php-stomp (1.0.5-1) unstable; urgency=low

  * Initial Release (Closes: #709514).

 -- Jonas Genannt <jonas.genannt@capi2name.de>  Fri, 24 May 2013 10:31:28 +0200
